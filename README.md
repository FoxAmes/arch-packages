# FoxAmes' Arch Packages
This is a collection of eclectic packages I've put together for a myriad of reasons.  They are built automatically and uploaded to GitLab's package repository.
