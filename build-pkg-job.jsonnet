local build_metadata = import 'build-metadata.json';

{
  default: {
    image: 'archlinux:base-devel',
  },
  stages: [
    'build-pkgs',
    'upload-pkgs',
  ],
} + {
  ['build-' + pkg]: {
    stage: 'build-pkgs',
    before_script: [
      'echo "[multilib]" >> /etc/pacman.conf',
      'echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf',
      'pacman -Sy --noconfirm git',
      'useradd -m -d /build -g wheel build',
      'echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers',
      'mkdir -m 0700 -p $CI_PROJECT_DIR/output /build/sources /build/srcpackages /build/makepkglogs',
      'chown -R build: /build $CI_PROJECT_DIR/output',
    ],
    script: [
      'pushd packages/' + pkg,
      'su build -c "makepkg -scr --noconfirm --config $CI_PROJECT_DIR/makepkg.conf"',
      'popd',
    ],
    artifacts: {
      paths: [
        'output',
      ],
      untracked: false,
      when: 'always',
      expire_in: '30 days',
    },
    rules: [
      {
        'if': '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH',
      },
      {
        'if': '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH',
      },
    ],
  }
  for pkg in build_metadata.pkgs
  if pkg != ''
} + {
  ['upload-' + pkg]: {
    stage: 'upload-pkgs',
    needs: [
      {
        job: 'build-' + pkg,
        artifacts: true,
      },
    ],
    script: [
      'for pkg in output/*.tar.zst; do',
      "export pkgname=$(pacman -Qp $pkg | awk ' { print $1 } ')",
      "export pkgver=$(pacman -Qp $pkg | awk  ' { print $2 } ')",
      'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$pkg" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/$pkgname/$pkgver/$pkg"',
      'done',
    ],
    rules: [
      {
        'if': '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH',
      },
    ],
  }
  for pkg in build_metadata.pkgs
  if pkg != ''
}
